#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Author: Eric Parent <eric.parent@gerad.ca>

License: GPL v3.0 or later

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""

import os
import os.path
import sys
import fileinput
import re
import getopt
import pdb
import consts
import NetLine05
import PIG_AnalysisTools
import utils

#----------------------------------------------------------------------
#
#                         Module specific constants
#
#----------------------------------------------------------------------

output_dir = '/home/parent/projet/code/pig/PassengerItineraryGenerator/results'
table_file = os.path.join(output_dir, 'errors_table.txt')
consolidatedErrors = os.path.join(output_dir,'consolidatedErrors.txt')

#----------------------------------------------------------------------
#
#                         Module specific methods
#
#----------------------------------------------------------------------

def usage():
    """Describes the usage of this script, with arguments
    descriptions."""
    print
    print 'Please use one of the following arguments in this manner:'
    print
    print ' --pig='
    print '       The file with passenger itineraries being generated from '
    print '       the "PassengerItineraryGenerator" (PIG) program.'
    print
    print ' --netline='
    print '       The file with passenger itineraries from NetLine (2005).'
    print
    print ' --table='
    print '       A name for the file which will hold the "table" information.'
    print
    print ' --testing'
    print '       Allows for running the parser on a NetLine file and'
    print '       displays itineraries on screen.'
    print
    print ' --help, -h'
    print '       Displays this message.'
    print
    return consts.SUCCES


def parse_arguments(inArgs):
    """Parses the options and return the values in a dictionnary.

    The fields are the following:

    Returns a dictionnary with the appropriate values for options
    (keys of the dictionnary)."""
    options = 'help netline= pig= table= testing'
    optionsList = options.split()
    shortOptions = 'h'
    try:
        optlist, args = getopt.getopt(inArgs, shortOptions, optionsList)
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)
    if not optlist:
        usage()
        sys.exit(2)

    # Creation of dictionnary and assigning default values.
    arguments = {}
    arguments['netline_file'] = None
    arguments['pig_file'] = None
    arguments['bounds_file'] = None
    arguments['testing'] = False
    arguments['table_file'] = os.path.abspath(table_file)

    for option, answer in optlist:
        # help
        if option in ("-h", "--help"):
            sys.exit(usage())
        # demand-shelve
        elif option == '--netline':
            arguments['netline_file'] = os.path.abspath(answer)
        # pig-file
        elif option == '--pig':
            arguments['pig_file'] = os.path.abspath(answer)
        elif option == '--table':
            arguments['table_file'] = os.path.abspath(answer)
        elif option == '--testing':
            arguments['testing'] = True
        else:
            usage()
            assert False, "unhandled option"
    return arguments


def load_service_ratio(fileName):
    serviceRatio = r'Airport_[A-Z]{3}/Airport_[A-Z]{3} is served at *'
    servicePattern = re.compile(serviceRatio)
    serviceDict = {}
    for line in fileinput.input(fileName):
        if servicePattern.match(line):
            content = [i.strip() for i in line.split(" is served at ")]
            key = content[0]
            val = content[-1]
            serviceDict[key] = float(val)
    fileinput.close()
    return serviceDict


def compare(file):
    compteur = 0
    print '- Lecture du fichier %s ' % str(file)
    for line in fileinput.input(file):
        compteur += 1
        # if it is information about the "key"
        if lowBoundPattern.match(line):
            boundKey.append([i.strip() for i in line.split(":")][1])
        # if it is information about the "value"
        elif pattern.match(line):
            boundVal.append([i.strip() for i in line.split(":")][0])
        # if it is overhead information...
        else:
            pass
    fileinput.close()
    print '- Lecture du fichier complétée...'
    print '- Lu %d lignes en tout' % compteur
    keySet = set(boundKey)
    valSet = set(boundVal)
    print
    print "Cardinalité de l'ensemble des clés    : %d" % len(keySet)
    print "Cardinalité de l'ensemble des valeurs : %d" % len(valSet)
    if len(keySet) > len(valSet) :
        print "Une différence entre les ensembles existe..."
        print "Différence de %d" % (len(keySet) - len(valSet))
        print "Voici les différences:"
        for i in (keySet - valSet):
            print str(i)
    elif len(keySet) < len(valSet) :
        print "Une différence entre les ensembles existe..."
        print "Différence de %d" % (len(valSet) - len(keySet))
        print "Voici les différences:"
        for i in (valSet - keySet):
            print str(i)
    return 0


def getExecutionTime(argsDict):
    fName = argsDict['pig_file'] = None
    expression = '- Time required by algorithm ='
    exec_time = "0.0"
    for line in fileinput.input(pigFile):
        if expression in line:
            (head, tail) = line.split("=")
            exec_time = tail.strip().split(" ")
    return float(exec_time)


def addExecutionTimeInfo(fName):
    f = open(fName, "a+")
    if f:
        f.write("Execution time of algorithm = %.3f\n"
                % getExecutionTime(argsDict))
    f.close()


def main(argsDict):
    """
    Input:
    - args is a dictionnary with the arguments values
    """

    output_dir = os.path.join('/home/parent/projet/code/pig',
                              '/PassengerItineraryGenerator/results')
    
    consolidatedErrors = os.path.join(output_dir,'consolidatedErrors.txt')
    #
    # 1 - Instantiate a DemandCompiler object to compile all the data.
    #
    print "-> Instantiation of a DemandCompiler object..."
    demandCompiler = PIG_AnalysisTools.DemandCompiler(argsDict)
    #
    # 2 - Build the information table
    #
    print "->  Building the demands table..."
    table = demandCompiler.buildTable()
    #demandCompiler.displayTable(table)
    #
    # 3 - Save the table in a file for later references.
    #
    tableFile = argsDict['table_file']
    demandCompiler.saveTable(table, tableFile)
    #
    # 4 - Display itineraries which are below a certain threshold
    #
    threshold = 85 # This value represents a demand ratio in %


#--- End of module specific methods


# Main execution of the script
#
if __name__ == "__main__" : 
    sys.exit( main( parse_arguments(sys.argv[1:]) ) )

