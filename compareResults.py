#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Author: Eric Parent <eric.parent@gerad.ca>

License: GPL v3.0 or later

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""

import os
import sys
import getopt
import fileinput
import utils
import NetLine05
import exceptions
import consts
import PIG_AnalysisTools


#### Script methods ####

def argumentFiles():
    """@brief Gets the shelve file and pig file to be processed.  Some
    routine checks about these files and returns them.

    @throw exceptions.IOError if invalid files were given.

    """
    if len(sys.argv) != 3:
        sys.exit(usage())
    pigFile = sys.argv[1]
    shelveFile = sys.argv[2]
    if not utils.isValidFile(pigFile):
        raise exceptions.IOError, \
            'Invalid file. Please provide with a valid file.'
    return str(pigFile), str(shelveFile)


def usage():
    """
    Describes the usage of this script, with arguments descriptions.
    """
    print
    print 'Please use one of the following arguments in this manner:'
    print
    print ' --itin-shelve'
    print '       Designates the itineraries shelve file.'
    print
    print ' --pig-file='
    print '       Designated thePIG file with generated itineraries.'
    print
    print ' --display-itins, -d'
    print '       Tells the program to show the itineraries.'
    print
    print ' --demand-shelve='
    print '       Designated the total demand shelve file.'
    print
    print ' --help, -h'
    print '       Displays this message.'
    print
    return 0


def parseOptions(inArgs):
    """
    @brief Parses the options and return the values in a dictionnary.
    The fields are the following:

    'itinerariesShelveFile' the shelve file containing all the
    itineraries from NetLine (2005)

    'demandShelve' the demand shelve file (with total demand per
    Origin/Destination)

    'pigFile'           the PassengerItineraryGenerator output file
    'showItineraries'   (boolean) telling wether to show the itineraries or not
    'outputFile'        the file to which writing the information


    @return a dictionnary with the appropriate values for options
    (keys of the dictionnary).

    """
    options = 'display-itins output-file= itin-shelve=' \
        ' pig-file= demand-shelve= help'
    optionsList = options.split()
    shortOptions = 'hd'
    try:
        optlist, args = getopt.getopt(inArgs, shortOptions, optionsList)
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)
    if not optlist:
        usage()
        sys.exit(2)
    arguments = {}
    arguments['itinerariesShelveFile'] = None
    arguments['demandShelve'] = None
    arguments['pigFile'] = None
    arguments['showItineraries'] = False
    arguments['outputFile'] = None
    for option, answer in optlist:
        if option in ("-h", "--help"):
            sys.exit(usage())
        elif option in ("-d", "--display-itins" ):
            arguments['showItineraries'] = True
        elif option == "--itin-shelve":
            arguments['itinerariesShelveFile'] = answer
        elif option == '--pig-file':
            arguments['pigFile'] = answer
        elif option == '--output-file':
            arguments['outputFile'] = answer
        elif option == '--demand-shelve':
            arguments['demandShelve'] = answer
        else:
            usage()
            assert False, "unhandled option"
    return arguments


def main(argsDict):
    """

    @brief 

    @param argsDict, a dict containing all the options (keys) and
    values (values) for this application.

    """
    shelveFile = argsDict['itinerariesShelveFile']
    demandShelveFile = argsDict['demandShelve']
    pigFile = argsDict['pigFile']
    showItineraries = argsDict['showItineraries']
    outputFile = argsDict['outputFile']

    comparator = PIG_AnalysisTools.Comparator()
    if pigFile:
        comparator.PIGdata = PIG_AnalysisTools.loadPIGfile(pigFile)
    if shelveFile:
        comparator.NLdata = utils.loadShelve(shelveFile)

    # Print the itineraries that were not generated
    #
    NetLine = set(comparator.referenceAsList())
    PIG = set(comparator.generatedAsList())
    
    # Itineraries that were not found in the
    # reference set of itineraries
    #
    newItins = list(PIG - NetLine)  # difference
    misses = list(NetLine - PIG)    # difference
    hits = list(NetLine & PIG)      # Intersection of these two lists
    
    print
    print 'Number of itineraries in reference = %d' % len(NetLine)
    print 'Number of hits   =  %d' % len(hits)
    print 'Number of misses =  %d' % len(misses)
    print 'Hit ratio = %6.2f ' % (100.0 * (len(hits))/float(len(NetLine))),
    print ' %'
    print 'Number of itineraries not in ref = %d ' % len(newItins)
    print 
    if showItins:
        if misses:
            print '################################'
            print '### A few itineraries missed ###'
            for item in misses:
                print str(item)
            print
            if newItins:
                print '#######################'
                print '### NEW ITINERARIES ###'
                for item in newItins:
                    print str(item)
            print
    return consts.SUCCES
    


if __name__ == "__main__" : 
    """
    Performs the comparison of the generated itineraries from PIG module
    where they have been processed and saved in a "shelve".
    The reference itineraries are found in the NetLine filel

    """
    sys.exit( main( parseArguments(sys.argv[1:]) ) )

##############################################################
