#! /usr/bin/python
# -*- coding: utf-8 -*-
"""Author: Eric Parent <eric.parent@gerad.ca>

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""
import os
import sys
import shelve
import exceptions


#####
def validateFile(file):
    if not os.path.exists(file):
        print 'File to be parsed does not exists.', 
        print 'Please give a valide file to be parsed.'
        sys.exit(1)

    if not os.path.isfile(file):
        print 'File given is not a file. ',
        print 'Please indicate a file to be parsed.'
        sys.exit(1)


#####
def isValidFile(file):
   """
   Validates the file to exists and to be a file,
   i.e. not a directory.

   Returns a boolean indicating if the file is
   minimally valid.
   """
   if not os.path.exists(file) or not os.path.isfile(file):
      return False
   return True


#####
def saveDict(filename, Dict):
   """
   Saves a dictionnary content into a designated file.
   
   @param filename un nom de fichier à créer

   @param a list of dictionnaries

   @post filename file is created and contains the information
   contained in Dict.
   """
    f = open(filename, "w")
    keys = Dict.keys()
    keys.sort()
    for key in keys:
        content = Dict[key]
        if hasattr(content, '__iter__'):
            f.write(str(key) + ' : ')
            # it is iterable...
            str_content = [str(i) for i in content]
            f.write('; '.join(str_content))
        else:
            f.write('%s : %s' % (str(key), str(content)))
        f.write('\n')
    f.close()


#####
def removeTupleListDuplicates(W):
    """@param W une liste qui comporte peut-être plusieurs fois le
    même élément

    """
    W.sort()
    last = W[-1]
    for i in range(len(W) - 2, -1, -1):
        if last == W[i]:
            del W[i]
        else:
            last = W[i]
    for i in W:
        (a, b) = i
        k = (b, a)
        if k in W:
            W.remove(k)
    return W


def removeListDuplicates(List):
    """
    @param List a list which might contain duplicates.
    
    @return a list with one and only one occurence of the elements of List,
    i.e. List with its duplicates removed.
    """
    List.sort()
    last = List[-1]
    for i in range(len(List) - 2, -1, -1):
        if last == List[i]:
            del List[i]
         else:
            last = List[i]
    return List


#####
def listDirectory(directory, fileExtList):
   """
   Returns a list of files for which the extension figures in a given
   list (fileExtList) and are located in a directory (directory).

   @param directory a string indicating the directory where to perform
   the search
   @param fileExtList a list of strings representing the file extensions to
   retain

   @return a list of files, with full path

   This function is taken from the book "Dive Ito Python" by Mark Pilgrim.
   """
   fileList = [os.path.normcase(f) for f in os.listdir(directory)]
   fList = [os.path.join(directory,f) \
               for f in fileList if os.path.splitext(f)[1] in fileExtList]
   fList.sort()
   return fList


#####
def saveShelve(filename, info):
   """Saves the info in dictionary named 'info' into a shelve
   named 'filename'.

   @param filename a name fot the database file (shelve type).

   @param info a dictionary which contains information to be
   stocked into a shelve file.

   @post the filename file is created and now contains the
   information contained into the info dictionary.
   """
   database = shelve.open(filename)
   if not isinstance(info, type({})):
      raise exceptions.ValueError, 'Please provide a dictionnary '\
          'for saving in the shelve.'

   for key in info.keys():
      database[str(key)] = info[key]

   database.close()


#####
def loadShelve(filename):
   """
   Loads the information in a shelve file.

   Returns a dictionnary based on the information found on this shelve.
   """
   database = shelve.open(filename)
   dbContent = dict(database)
   database.close()
   return dbContent


######## End of utils module ########
