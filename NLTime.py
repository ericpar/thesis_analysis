#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
@package NLTime

Module containing classes for parsing and manipulating time info as
can be found in NetLine 2005 itineraries files.

Author: Eric Parent <eric.parent@gerad.ca>

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""

import re
import exceptions


#-----------------------------------------------------
#
#  Class NL05Line
#
#-----------------------------------------------------

class NLTime():
   """
   """
   MAX_NB_MINUTES = 1440 # = 23*60 + 59

   def __init__(self, time_literal=""):
      """
      Constructor.
      """
      self.hours = 0
      self.minutes = 0
      self.nbMinutes = self.getNbOfMinutes(self.hours, self.minutes)
      if time_literal:
         if not self.__isValid(time_literal):
            raise exceptions.ValueError, \
                "Invalid value given to constructor of NLTime class."
         self.literal = time_literal
         self.hours, self.minutes, self.nbMinutes = self.__parse(self.literal)


   def toLong(self, time_literal=""):
      if time_literal:
         if not self.__isValid(time_literal):
            raise exceptions.valueError, \
                "Invalid time literal given to NLTime."
         self.literal = time_literal
         hours, minutes, nbMinutes = self.__parse(time_literal)
      return self.nbMinutes


   def toLiteral(self, hours=None, minutes=None):
      format = "%02d%02d"
      if hours is not None and minutes is not None:
         if not self.hoursWithinRange(hours) \
                or not self.minutesWithinRange(minutes) \
                or not self.__isValid(format % (hours, minutes,)):
            raise exceptions.ValueError, "invalid format"
         fields = (hours, minutes,)
      else:
         fields = (self.hours, self.minutes,)
      return format % fields


   def __parse(self, time_literal):
      if not self.__isValid(time_literal):
         raise exceptions.ValueError, 'Invalid format'
      hours = int(time_literal[:2])
      minutes = int(time_literal[2:])
      nbMinutes = self.getNbOfMinutes(hours, minutes)
      if not self.nbMinutesWithinRange(nbMinutes):
         raise exceptions.ValueError, \
             'Invalid time literal given to NLTime.'
      return (hours, minutes, nbMinutes,)


   def getNbOfMinutes(self, hours=0, minutes=0):
      if self.hoursWithinRange(hours) \
             and self.minutesWithinRange(minutes):
         return hours*60 + minutes
      else:
         print "hours: %02d, minutes: %02d" % (hours, minutes,)
         raise exceptions.ValueError, \
             "Invalid numerical values given to NLTime.getNbOfMinutes()."


   def nbMinutesWithinRange(self, nbMin):
      max_value = 23 * 60 + 59
      return self.__withinRange(nbMin, 0, NLTime.MAX_NB_MINUTES-1)


   def hoursWithinRange(self, hours):
      return self.__withinRange(hours, 0, 23)


   def minutesWithinRange(self, minutes):
      return self.__withinRange(minutes, 0, 59)


   def __withinRange(self, value, minVal=0, maxVal=59):
      if value <= maxVal and value >= minVal:
         return True
      return False


   def __isValid(self, time_literal):
      """
      Expects the time literal to be of the form '0745' for 7:45 AM
      and '1642' for 4:42 PM.

      @param time_literal a string representing a time literal as
      described previously.

      @return a boolean answer.
      """
      expression = r'[0-9]{4}'
      pattern = re.compile(expression)
      if pattern.match(time_literal):
         return True
      return False


   def __add__(self, other):
      nbMins = (self.nbMinutes + other.nbMinutes) % NLTime.MAX_NB_MINUTES
      if nbMins < 0: nbMins += NLTime.MAX_NB_MINUTES
      return self.__copyFromNbMins(nbMins)


   def __sub__(self, other):
      nbMins = (self.nbMinutes - other.nbMinutes) % NLTime.MAX_NB_MINUTES
      if nbMins < 0: nbMins += NLTime.MAX_NB_MINUTES
      return self.__copyFromNbMins(nbMins)


   def __copyFromNbMins(self, nbMins):
      hours = nbMins / 60
      minutes = nbMins % 60
      return NLTime(self.toLiteral(hours, minutes))


   def toString(self):
      return self.toLiteral()


   def __str__(self):
      return self.toString()


   def __eq__(self, other):
      return self.toLong() == other.toLong()


#
#-- End of "class NL05Line"

