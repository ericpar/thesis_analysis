#! /usr/bin/python
# -*- coding: utf-8 -*-

"""Author: Eric Parent <eric.parent@gerad.ca>

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""

import os
import sys
import fileinput
import shelve
from constantes import *

############# Définitions de fonctions #############
def removeTupleListDuplicates(W):
   """
   @param W une liste qui comporte peut-être plusieurs fois le même élément
   """
   #d = {}
   #for x in W: d[x] = x
   #W = [d.values()]
   W.sort()
   last = W[-1]
   for i in range(len(W)-2, -1, -1):
       if last == W[i]:
          del W[i]
       else:
          last = W[i]
   for i in W:
      (a,b) = i
      k = (b,a)
      if k in W: W.remove(k)
   return W



def removeListDuplicates(List):
   """
   @param Liste une liste qui comporte peut-être plusieurs fois le même élément
   """
   List.sort()
   last = List[-1]
   for i in range(len(List)-2, -1, -1):
       if last == List[i]:
          del List[i]
       else:
          last = List[i]
   return List



def loadRoutes(fIn):
   """ Lit le fichier dont le nom est fIn et retourne une liste formée des séquences lues dans le fichier de séquences.

   Entrée: un nom de fichier à traiter.
   Retour: une liste de dictionnaires correspondant à chacune des lignes du fichier"""
   contenu = []
   for ligne in fileinput.input(fIn):
      ligne.strip() # Remove all the blanks at beginning and end of string
      d = parseLine(ligne)
      contenu.append(d)
   return contenu 


def loadFlights(fIn):
   """
   Lit le fichier dont le nom est fIn et retourne une liste formée des séquences lues dans le fichier de séquences.

   @param fIn a file to read
   @return a list of dictionnaries where each dictionnary corresponds to a flight legs informations
   """
   content = []
   for line in fileinput.input(fIn):
      line.strip()
      d = parseFlights(line)
      content.append(d)
   return content 



def load_ODs(OD, fIn):
   """
   Lit le fichier dont le nom est fIn et extrait toutes les paires (O,D) qui existent dans le réseau.

   Entrée: un nom de fichier à traiter.
   Retour: une liste de (O,D)
   """
   for ligne in fileinput.input(fIn):
      ligne.strip() # Remove all the blanks at beginning and end of string
      d = parseLine(ligne)
      # Ne conserver que les (O,D) pour lesquelles il y une demande
      # (BASE représente la demande non-contrainte ou "unconstrained demand")
      if d["BASE"] > 0.0:
         couple = ( d["Origine"] , d["Destination"] )
         OD.append( couple )
   return( removeTupleListDuplicates(OD) )



def save_ODs(filename, OD):
   """
   Sauvegarde les données des OD utiles dans un fichier ASCII pour utilisation future
   @param filename un nom de fichier à créer
   """
   f = open(filename, "w")
   for i in OD:
      f.write( str(i) )
      f.write("\n")
   f.close()



def parseLine(ligne):
   """ Lit le contenu d'une ligne d'un fichier d'itinéraires et le stocke dans un dictionnaire.
   Retourne le dictionnaire renseigné."""
   contenu = {}
   debut = 0
   fin = 8
   
   for i in range(1,3):
      contenu["Flight" + str(i)] = ligne[debut:fin].strip()
      debut = fin
      fin = fin+8

   contenu["Origine"] = ligne[24:27].strip()
   contenu["Connection #1"] = ligne[27:31].strip()
   contenu["Connection #2"] = ligne[31:35].strip()
   contenu["Destination"] = ligne[35:39].strip()
   contenu["Departure time"] = ligne[39:44].strip()
   contenu["Arrival time"] = ligne[44:49].strip()
   contenu["Elapsed time"] = ligne[49:54].strip()
   contenu["Plane 1"] = ligne[54:58].strip()
   contenu["Plane 2"] = ligne[58:62].strip()
   contenu["Plane 3"] = ligne[62:66].strip()
   contenu["Day (of week)"] = ligne[66:71].strip()
   contenu["BASE"] = ligne[71:76].strip()
   contenu["Dem"] = ligne[76:82].strip()
   contenu["Rev"] = ligne[82:89].strip()
   contenu["# of legs"] = ligne[89:91].strip()

   contenu["Leg1"] = ligne[91:105].strip()
   contenu["Leg2"] = ligne[105:119].strip()
   contenu["Leg3"] = ligne[119:134].strip()
   contenu["Leg4"] = ligne[134:148].strip()
   contenu["Leg5"] = ligne[148:161].strip()

   contenu["Spill"] = ligne[161:169].strip()
   contenu["Rec"] = ligne[169:175].strip()
   contenu["Mark"] = ligne[175:181].strip()

   contenu["AR1"] = ligne[181:186].strip()
   contenu["DE1"] = ligne[186:191].strip()
   contenu["AR2"] = ligne[191:196].strip()
   contenu["DE2"] = ligne[196:].strip()

   return (contenu)



def parseFlights(line):
   """
   Lit le contenu d'une ligne d'un fichier d'itinéraires et le stocke dans un dictionnaire.
   Retourne le dictionnaire renseigné.
   """
   content = {}
   start = 0
   end = 8
   #for i in range(1,3):
   #   content["Flight" + str(i)] = line[start:end].strip()
   #   start = end
   #   end = end + 8
   content["Origine"] = line[24:27].strip()
   content["Destination"] = line[35:39].strip()
   content["Departure time"] = line[39:44].strip()
   content["Arrival time"] = line[44:49].strip()
   content["Elapsed time"] = line[49:54].strip()
   content["Leg1"] = line[91:105].strip()
   content["Leg2"] = line[105:119].strip()
   content["Leg3"] = line[119:134].strip()
   return (content)



def listDirectory(directory, fileExtList):
   """
   Retourne une liste des fichiers dont l'extention figure dans la
   liste d'extensions "fileExtList" et qui se trouvent dans le
   répertoire "directory".

   PARAMÈTRES:
   - directory une chaîne de caractères désignant un répertoire informatique
   - fileExtList une liste de chaînes de caractères représentant des extensions de fichiers

   RETOUR:
   une liste de chaînes de caractères (fichiers)
   """
   fileList = [os.path.normcase(f) for f in os.listdir(directory)]
   fList = [os.path.join(directory,f) for f in fileList if os.path.splitext(f)[1] in fileExtList]
   fList.sort()
   return fList


def displayContent(contenu):
   """
   Affiche le contenu d'une liste de dictionnaires.
   """
   k = 1
   print "type of contenu:" + str(type(contenu))
   print len(contenu)

   for data in contenu:
      print "type of data:" + str(type(data))
      print "--------------------------------"
      print "Line #"+str(k)
      k = k + 1
      infos = []

      for key in data.keys():
         infos.append(key+" : "+data[key])
         infos.sort()

         for item in infos:
            print item

   print "\n\n"



def extractFlights(fichiers, flights):
   """
   Extracts the flights informations (preliminary) about the legs (start and end points of each legs).

   @param fichiers a list of files containing the flights informations
   @return a dictionnary contaning a list of its legs (start,end)
   """
   champs = ["Leg1","Leg2","Leg3"]
   for f in fichiers:
      infos = loadFlights(f)
      for item in infos:
         for champ in champs:
            info = item[champ].strip()
            if info != BLANK_STR:
               nVol = info[6:]
               a = info[0:3]
               b = info[3:6]
               if not flights.has_key(nVol):
                  flights[nVol] = [(a,b)]
               else:
                  if (a,b) not in flights[nVol]:
                     flights[nVol].append( (a,b) )
   return flights


def saveDict(filename, Dict):
   """
   Saves a dictionnary content into a designated file.
   
   @param filename un nom de fichier à créer
   @param a list of dictionnaries
   """
   total_legs = 0
   f = open(filename, "w")
   keys = Dict.keys()
   keys.sort()
   for key in keys:
      f.write( str(key) + " : " + str(Dict[key]) + "\n" )
      total_legs += len(Dict[key])
   f.close()
   print "\nTotal number of legs = " + str(total) + "\n"



def displayFlights(vols):
   """
   Displays the flights informations contained in a dictionnary.
   """
   keys = vols.keys()
   keys.sort()

   for key in keys:
      print str(key) + " : " + str(data[key])



##############################################################
###-------------------- main program ----------------------###
##############################################################

if __name__ == "__main__" : 

   files = listDirectory("/home/parent/projet/code/pig/SmallScenario2005", [".in"])
   flights = extractFlights(files, {})
   fs = os.path.join("/home/parent/projet/code/pig/PassengerItineraryGenerator/analysis","SmallShelve.dat")
   saveDict(fs, flights)

   sys.exit(0)

###############################
