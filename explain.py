#! /usr/bin/python
# -*- coding: utf-8 -*-
"""@package explain

This module provides with various capabilities for parsing information
related to the PassengerItineraryGenerator (PIG) program and compare
two sets of itineraries being generated.

Author: Eric Parent <eric.parent@gerad.ca>

License: GPL v3.0 or later

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""


import os
import sys
import getopt
import fileinput
import re
import subprocess
import pdb
import utils
import NetLine05


#-----------------------------------------------------------------
#
#  Script's methods and attributes
#
#-----------------------------------------------------------------


def countItineraries(data):
    """Counts the itineraries in the data dict and returns an
    integer."""
    count = 0
    for key in data.keys():
        count += len(data[key])
    return count



def referenceAsList(data):
    """Returns the reference itineraries in a list."""
    L = []
    for key in data.keys():
        L.extend(data[key])
    L = list(set(L))
    return L



def intersection(data1, data2):
    """
    Returns the intersection of the PIG itineraries and the
    reference itineraries.
    """
    in_both = {}

    for key in data1.keys():

        if data2.has_key(key):

            left = set(data1[key])
            right = set(data2[key])

            in_both[key] = list(left & right) # compute the intersection

    return in_both



def referenceAsList(data):
    """Returns the reference itineraries in a list."""
    L = []
    for key in data.keys():
        L.extend(data[key])
    L = list(set(L))
    return L



def differenceList(A, B):
    setA = set(referenceAsList(A))
    setB = set(referenceAsList(B))
    in_both = list(setA - setB)
    return in_both



def difference(A, B):
    """Returns the difference (set operation) of the PIG
    itineraries and the reference itineraries."""
    in_both = {}
    for key in A.keys():
        if B.has_key(key):
            setA = set(A[key])
            setB = set(B[key])
            # compute the intersection
            in_both[key] = list(setA - setB)
    return in_both



def processPIGFile(pigFile):
    """Load data contained on a line matching a regular expression
    corresponding to an itinerary found in the output from
    PassengerItineraryGenerator program.

    The pattern to match is the following:
    'YUL/VVR -> '

    where YUL and VVR are example of airport IATA identifiers.

    Returns a dictionnary with keys being the Origin/Destination
    identifier and the value being a list of itineraries (string) in
    the following format:

    >>> data = loadPIGfile(pig_file_name)
    >>> for j in data.keys():
    ...     print j
    ...     for k in data[j]:
    ...         print k
    ...

    Otherwise stated (example):

    >>> data[ j ][0]
    'Leg_AC_1231_YUL_3, Leg_AC_432_YVR_3'
    >>> data[j+1][0]
    'Leg_AC_441_YYX_1'
    >>> data[j+2][0]
    'Leg_AC_83_YVR_2, Leg_AC_8687_YUL_3, Leg_AC_554_YUQ_3'
    ...
    """
    print '-> Loading generated itineraries found in file:',
    print os.path.abspath(pigFile)

    #expression = r'Airport_[A-Z]{3}/Airport_[A-Z]{3} -> *'

    expression = r'[A-Z]{3}/[A-Z]{3} -> *'   ## enlevé 'Airport...'
    pattern = re.compile(expression)
    data = {}

    for line in fileinput.input(pigFile):
        if pattern.match(line):
            #print "Matched line: %s" % line
            lineData = [i.strip() for i in line.split('->')]
            key = lineData[0].strip()
            remainder = lineData[1:]
            itin = ", ".join([leg.strip() for leg in remainder])
            if key not in data.keys():
                data[key] = [itin]
            else:
                data[key].append(itin)
                X = data[key]
                data[key] = list(set(X))

    print "-> Loading is completed : parsed %d itineraries for %d (O,D) pairs" % (countItineraries(data), len(data.keys()),)
    print
    return data



def usage():
    """
    Describes the usage of this script, with arguments descriptions.

    """
    print 'Please use one of the following arguments in this manner:'
    print
    print ' --file1'
    print '    File containing itineraries to parse.'
    print
    print ' --file2'
    print '    File containing itineraries to parse.'
    print
    print ' --diff'
    print '    itineraries being displayed are given by file1 \ file2'
    print
    print ' --intersect'
    print '    itineraries found in file1 and also in file2'
    print
    print ' --list, -l'
    print '    displays the list difference of list(file1) - list(file2)'
    print
    print ' --help, -h'
    print '     Displays this message.'
    print
    return 0




def parseArguments(inputArgs):
   """
   Parses the given list of arguments (string) given by inputArgs and
   returns a dictionnary with  corresponding key/values.
   """
   options = 'list file1= file2= help intersect diff'
   optionsList = options.split()
   shortOptions = 'hidl'

   try:
      optlist, args = getopt.getopt(inputArgs, shortOptions, optionsList)

   except getopt.GetoptError, err:
      print str(err)
      usage()
      sys.exit(2)

   if not optlist:
      usage()
      sys.exit(2)

   arguments = {}
   arguments['file1'] = None
   arguments['file2'] = None
   arguments['diff'] = False
   arguments['intersect'] = False
   arguments['list'] = False

   for option, answer in optlist:

      if option in ("-h", "--help"):
         sys.exit(usage())

      elif option == '--file1':
         arguments['file1'] = answer

      elif option == '--file2':
         arguments['file2'] = answer

      elif option in ('-d', '--diff') :
          arguments['diff'] = True

      elif option in ('-l', '--list') :
          arguments['list'] = True

      elif option in ('-i', '--intersect'):
          arguments['intersect'] = True

      else:
         print "Option %s not recognized..." % option
         assert False, "unhandled option"

   return arguments



def main(inputArgs):
    key1 = "file1"
    key2 = "file2"

    file1 = inputArgs[key1]
    file2 = inputArgs[key2]

    print "* loading " + key1
    data1 = processPIGFile(file1)

    print "* loading " + key2
    data2 = processPIGFile(file2)

    if inputArgs['intersect'] :

        in_both = intersection(data1, data2)
        print "-" * 40
        print "(O,D) found in both: %s" % in_both.keys()
        print

    if inputArgs['diff'] :

        diff = difference(data1, data2)
        #diff = differenceList(data1, data2)
        #diff = list(set(data1.values()) - set(data2.values()))
        
        #print "-" * 40
        #print "-" * 40
        #print "(O,D) in difference:"
        #for key in diff.keys():
        #    print str(key)

        print "-" * 40
        print "-" * 40
        for key in diff.keys():
            print "Itineraries found in set #1 and not in set #2 for (O,D) = (%s):" % key.replace("/", ", ")
            for itin in diff[key]:
                print str(itin)
        #for item in diff:
        #    print str(item)
        print "-" * 40
        print

        print "(O,D) adressed in 'file1' but not in 'file2':"
        d = display_A_minus_B(data1, data2)
        print "counting %d elements\n" % len(d)

        print
        print "(O,D) adressed in 'file2' but not in 'file1':"
        d = display_A_minus_B(data2, data1)
        print "counting %d elements\n" % len(d)

    if inputArgs['list'] :
        print "-" * 40
        print "-" * 40
        diff = differenceList(data1, data2)
        print "counting %d elements\n" % len(diff)

    return 0



def display_A_minus_B(dataA, dataB):
    """
    """
    setA = set(dataA.keys())
    setB = set(dataB.keys())
    diff = list(setA - setB)
    print "display_A_minus_B(dataA, dataB):"
    for item in diff:
        print str(item)
    return diff



#------------------------------------------------------------#
if __name__ == "__main__" : 
   args = parseArguments(sys.argv[1:])
   sys.exit(main(args))


#----------------------------------------------------------------------
if __name__ == "__main__" :
    sys.exit( main( parseArguments(sys.argv[1:]) ) )

#----------------------------------------------------------------------
