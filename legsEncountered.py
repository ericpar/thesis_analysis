#! /usr/bin/python
# -*- coding: utf-8 -*-
"""Author: Eric Parent <eric.parent@gerad.ca>

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""

import os
import sys
import getopt
import fileinput
import exceptions



class LegsAccountant:
    """
    Takes note and gives information about legs encountered in the creation
    of the Passenger Itineraries Generator.
    """


    def __init__(self):
        """
        """
        self.__file = None
        self.__legs = None


    def legs(self):
        """
        """
        return self.__legs.copy()



    def legsFile(self, file=None):
        """
        Getter/Setter for the encountered legs file data member.
        """
        if file is None:
            return self.__file
        else:
            self.__file = file



    def loadFile(self, legsFile=None):
        """
        """
        if legsFile is None:
            if self.__file:
                file = self.__file
            else:
                raise exceptions.IOError, "No file to read!"
        else:
            self.__file = legsFile
            legs = {}
            for line in fileinput.input(self.__file):
                legName = line.strip()
                if not legs.has_key(legName):
                    legs[legName] = 1
                else:
                    nbOccurences = legs[legName]
                    legs[legName] = nbOccurences + 1
            # rendu ici !!!
        self.__legs = legs
        return legs



    def maxNbOfOccurences(self):
        """
        Returns the maximum number of occurences for any leg encoutered.
        """
        if not self.__legs:
            return -1
        M = 0
        for key in self.__legs.keys():
            M = max(M, self.__legs[key])

        return M



    def numberOfLegs(self, Legs=None):
        """
        Gives the number of distinct legs encountered.
        """
        if Legs is None:
            if self.__legs is not None:
                return len(self.__legs)
            else:
                return 0
        else:
            return len(Legs)



    def computeStdDev(self, legs=None):
        """
        Goes through the dictionnary legsOccurences and computes the average
        of occurences of a leg.

        Returns the average number of occurences per leg.

        >>> D = {}
        >>> D["Leg1"] = 3
        >>> D["Leg2"] = 4
        >>> D["Leg3"] = 2
        >>> D["Leg4"] = 2
        >>> LegsAccountant.computeStdDev(D)
        3
        """
        if legs is None:
            if self.__legs is not None:
                legs = self.__legs

        nbItems = self.numberOfLegs(legs)

        if nbItems == 0:
            return 0.0

        count = 0
        for key in legs.keys():
            count += legs[key]

        # Computes the average value of occurences for each leg
        S = float(count) / float(nbItems)
        return S

#
### End of class LegsAccountant



############################## Module's methods ##############################

def usage():
    """
    Describes the usage of this script, with arguments descriptions.
    """
    print '-v Verbose mode: will display the itineraries encountered'\
        ' on standard output.'
    print '-f --file  describes the input file containing all' \
        ' the encountered legs.'
    print '-t --test  tests the modules and inner classes.'
    print '-h --help  displays this message.'
    return 0


def tests():
    """
    """
    errNo = 0
    ok = ' PASSED!'
    fail = ' FAILED!'
    accountant = LegsAccountant()

    print 'Testing LegsAccountant setter/getter for file (None): ',
    file = accountant.legsFile()
    if file is None:
        print ok
    else:
        print fail
        errNo += 1
    file = "voidLegsFile.txt"
    accountant.legsFile(file)
    print 'Testing LegsAccountant setter/getter for file: ',
    if accountant.legsFile() == file:
        print ok
    else:
        print fail
        errNo += 1

    print 'Testing LegsAccountant.computeStdDev() function.'
    D = {}
    msg = ' - for no legs occurences at all: '
    print msg,
    if 0 == accountant.computeStdDev(D):
        print ok
    else:
        print fail
        errNo += 1
    D["Leg1"] = 3
    D["Leg2"] = 4
    D["Leg3"] = 5
    D["Leg4"] = 4
    answer = accountant.computeStdDev(D)
    good = 4
    msg = ' - for a few legs occurences: '
    print msg,
    if 4 == accountant.computeStdDev(D):
        print ok
    else:
        print fail
        print 'Expected ' +str(good) + ', obtained ' + str(answer)
        errNo += 1

    print '-- End of tests --'
    return errNo



def runAccountant(legsFile, show):
    """
    """
    if legsFile is None:
        raise exceptions.ValueError, 'No file name was given for reading legs.'
        return 1
    accountant = LegsAccountant()
    accountant.loadFile(legsFile)
    print 'Total of %d differents legs were encoutered.'\
        % accountant.numberOfLegs()
    print 'Average of encountered occurences for each leg = %.2f'\
        % accountant.computeStdDev()

    if show:
        legs = accountant.legs()
        for key in legs.keys():
            print key

    return 0



def main():
    """
    """
    options = 'input= file= help test'
    optionsList = options.split()
    shortOptions = 'htdv'

    try:
        optlist, args = getopt.getopt(sys.argv[1:], shortOptions, optionsList)
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)

    if not optlist:
        print 'Please use one of the following arguments in this manner:'
        print
        usage()
        sys.exit(2)

    showItineraries = False

    for option, answer in optlist:
        if option in ("--help", "-h"):
            usage()
            return 0
        elif option == "-v":
            showItineraries = True
        elif option in ("--test", "-t"):
            return tests()
        elif option in ("-f", "--file", "--input"):
            return runAccountant(answer, showItineraries)
        else:
            assert False, "unhandled option"



if __name__ == "__main__" : 
    """
    Performs the comparison of the generated itineraries from PIG module
    where they have been processed and saved in a "shelve".
    The reference itineraries are found in the NetLine filel

    @pre

    @post

    """
    sys.exit(main())


##############################################################
