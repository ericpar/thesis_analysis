#! /usr/bin/python
# -*- coding: utf-8 -*-
"""Author: Eric Parent <eric.parent@gerad.ca>

License: GPL v3.0 or later

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""

# mailto:
#
# (c) Eric Parent, 2009
#

import os
import sys
import fileinput
import re


##### Utility functions
def compare(file):
    """
    Load data contained on a line matching a regular expression
    corresponding to an itinerary found in the output from
    PassengerItineraryGenerator program.

    The pattern to match is the following:

    'Airport_YUL/Airport_VVR -> '

    where YUL and VVR are example of airport IATA identifiers.

    Returns a list of itineraries (string) in the following format:

    myList[ j ] = 'Leg_AC_1231_YUL_3, Leg_AC_432_YVR_3'
    myList[j+1] = 'Leg_AC_441_YYX_1'
    myList[j+2] = 'Leg_AC_83_YVR_2, Leg_AC_8687_YUL_3, Leg_AC_554_YUQ_3'
    ...

    """
    expression = r'     Airport_[A-Z]{3}/Airport_[A-Z]{3}  : *'
    lowBoundExpr = r'Lower bound key: Airport_[A-Z]{3}/Airport_[A-Z]{3}'
    lowBoundPattern = re.compile(lowBoundExpr)
    pattern = re.compile(expression)
    boundKey = []
    boundVal = []
    compteur = 0
    print '- Lecture du fichier %s ' % str(file)
    for line in fileinput.input(file):
        compteur += 1
        # if it is information about the "key"
        if lowBoundPattern.match(line):
            boundKey.append([i.strip() for i in line.split(":")][1])
        # if it is information about the "value"
        elif pattern.match(line):
            boundVal.append([i.strip() for i in line.split(":")][0])
        # if it is overhead information...
        else:
            pass

    print '- Lecture du fichier complétée...'
    print '- Lu %d lignes en tout' % compteur
    keySet = set(boundKey)
    valSet = set(boundVal)
    print
    print "Cardinalité de l'ensemble des clés    : %d" % len(keySet)
    print "Cardinalité de l'ensemble des valeurs : %d" % len(valSet)
    if len(keySet) > len(valSet) :
        print "Une différence entre les ensembles existe..."
        print "Différence de %d" % (len(keySet) - len(valSet))
        print "Voici les différences:"
        for i in (keySet - valSet):
            print str(i)
    elif len(keySet) < len(valSet) :
        print "Une différence entre les ensembles existe..."
        print "Différence de %d" % (len(valSet) - len(keySet))
        print "Voici les différences:"
        for i in (valSet - keySet):
            print str(i)

    return 0


###################################################################

if __name__ == "__main__" : 

    sys.exit(compare(sys.argv[1]))
    
    #sys.exit( main( parseArguments(sys.argv[1:]) ) )

###################################################################
