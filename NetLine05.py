#! /usr/bin/python
# -*- coding: utf-8 -*-
"""@package NetLine05

Module containing classes for parsing and manipulating NetLine 2005
itineraries files.

Author: Eric Parent <eric.parent@gerad.ca>

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""

import os
import sys
import exceptions
import fileinput
import getopt
import utils
from PIG_AnalysisTools import *
from NLTime import NLTime


#-----------------------------------------------------
#
#  Class NL05Line
#
#-----------------------------------------------------

class NL05Line():
   """
   This class allows the parsing and manipulation of data
   contained on one line of NetLine2005 itineraries file.

   Since itineraries with more than 5 legs are considered to be
   marginals, they are not parsed by this program.

   """

   #--- Constants ---#
   Flight1 = 0
   Flight2 = 1
   Flight3 = 2
   Origin = 3
   Connection1 = 4
   Connection2 = 5
   Destination = 6
   DepartureTime = 7
   ArrivalTime = 8
   ElapsedTime = 9
   Plane1 = 10
   Plane2 = 11
   Plane3 = 12
   DayOfWeek = 13
   Base = 14
   Dem = 15
   Rev = 16
   NbOfLegs = 17
   Leg1 = 18
   Leg2 = 19
   Leg3 = 20
   Leg4 = 21
   Leg5 = 22
   Spill = 23
   Rec = 24
   Mark = 25
   AR1 = 26
   DE1 = 27
   AR2 = 28
   DE2 = 29
   AIRPORTS = 30
   LineLength = 31
   VoidLeg = 'ZZZZZZXXX9999'
   VoidTime = '0000'
   NaN = -9999
   NoAirport = 'ZZZ'
   Demand = Dem


   def __init__(self, line):
      """
      Constructor.

      This takes as input a line read from a NetLine (2005) itinerary
      file and parses its content.
      """
      self.line = range(NL05Line.LineLength)
      self.__parseLine(line)
      self.content = range(NL05Line.LineLength)
      key, itinerary = self.itineraryFromLine()
      self.key = self.getKey()
      self.itinerary = itinerary


   def originDestination(self):
      """
      Returns the origin/destination string.

      Such string looks like this: 'Airport_YUL/Airport_YVR'
      """
      return self.getKey()


   def getDemand(self):
      """
      Returns the float corresponding to the demand associated with this
      itinerary.

      Returns a float corresponding to the demand being satisfied by
      this itinerary.
      """
      return float(self.line[NL05Line.Dem])


   def __flightIndex(self, nb):
      if nb == 1:
         return NL05Line.Flight1
      elif nb == 2:
         return NL05Line.Flight2
      elif nb == 3:
         return NL05Line.Flight3
      else:
         return -1


   def __getitem__(self, i):
      """
      """
      if self.line:
         return self.line[i]
      else:
         raise "ERROR"
         return None


   def rawLine(self):
      return self.raw_line


   def __parseLine(self, line):
      """
      Lit le contenu d'une line d'un fichier d'itinéraires et le stocke
      dans un dictionnaire.
      
      Retourne le dictionnaire renseigné.
      """
      debut = 0
      fin = 8

      for i in range(1,4):
         self.line[self.__flightIndex(i)] = line[debut:fin].strip()
         debut = fin
         fin = fin + 8

      self.line[NL05Line.Origin] = line[24:27].strip()
      self.line[NL05Line.Connection1] = line[27:31].strip()
      self.line[NL05Line.Connection2] = line[31:35].strip()
      self.line[NL05Line.Destination] = line[35:39].strip()
      self.line[NL05Line.DepartureTime] = line[39:44].strip()
      self.line[NL05Line.ArrivalTime] = line[44:49].strip()
      self.line[NL05Line.ElapsedTime] = line[49:54].strip()
      self.line[NL05Line.Plane1] = line[54:58].strip()
      self.line[NL05Line.Plane2] = line[58:62].strip()
      self.line[NL05Line.Plane3] = line[62:66].strip()
      self.line[NL05Line.DayOfWeek] = line[66:69].strip()
      self.line[NL05Line.Base] = line[69:76].strip()
      self.line[NL05Line.Dem] = line[76:82].strip()
      self.line[NL05Line.Rev] = line[82:89].strip()
      self.line[NL05Line.NbOfLegs] = line[89:92].strip()
      self.line[NL05Line.Leg1] = line[92:105].strip()
      self.line[NL05Line.Leg2] = line[105:119].strip()
      self.line[NL05Line.Leg3] = line[119:133].strip()
      self.line[NL05Line.Leg4] = line[133:147].strip()
      self.line[NL05Line.Leg5] = line[147:161].strip()
      self.line[NL05Line.Spill] = line[161:169].strip()
      self.line[NL05Line.Rec] = line[169:175].strip()
      self.line[NL05Line.Mark] = line[175:181].strip()
      self.line[NL05Line.AR1] = line[181:186].strip()
      self.line[NL05Line.DE1] = line[186:191].strip()
      self.line[NL05Line.AR2] = line[191:196].strip()
      self.line[NL05Line.DE2] = line[196:201].strip()
      self.legs = [self.line[NL05Line.Leg1], \
                 self.line[NL05Line.Leg2], \
                 self.line[NL05Line.Leg3], \
                 self.line[NL05Line.Leg4], \
                 self.line[NL05Line.Leg5]]
      self.line[NL05Line.AIRPORTS] = self.visitedAirports(self.legs)

      nbOfLegs = int(self.line[NL05Line.NbOfLegs])
      if nbOfLegs > 5:
         self.line = None
      self.raw_line = line


   def connectionTimes(self):
      """
      Gets the connection times at all the stations.

      >> L = NL05Line(line_of_text)
      >> print L.connectionTimes()
      [ ('YUL', 46), ('YYC', 71) ]
      >>

      @return a list of tuples in the form 
      """
      connTimes = []
      nb_legs = int(self.line[NL05Line.NbOfLegs])
      # if there is only one leg in the itinerary, there is obviously
      # no connection time. Only consider when there is more than one
      # leg.
      if nb_legs > 1:
         time_fields = self.timeFields(nb_legs)
         for i in range(nb_legs - 1):
            # legs are numbered from 1 to 4 inclusively, hence an
            # offset of 1 is needed.
            idx = self.leg_index(i+1)
            leg_desc = self.line[idx]
            station = leg_desc[3:6]
            arrivalTime = NLTime(time_fields[i][1])
            departureTime = NLTime(time_fields[i+1][0])
            if not (arrivalTime == departureTime):
               waiting = departureTime - arrivalTime
               duration = waiting.toLong()
               connTimes.append( (station, duration,) )
      return connTimes


   def airportName(self, airportCode):
      """Returns the airport name as used in FlightScheduler module.

      For example, if airport code is 'YUL', the returned airport name
      is 'Airport_YUL'.
      """
      #return 'Airport_' + airportCode
      return airportCode
   

   def timeFields(self, nb_legs):
      indexes = self.timeFieldsIndexes(nb_legs, str_format=False)
      _fields = []
      for j, k in indexes:
         _fields.append((self.line[j], self.line[k],))
      return _fields


   def timeFieldsIndexes(self, nb_of_legs, str_format=False):
      f = int
      if str_format:
         f = str
      n = int(nb_of_legs)
      if n == 1:
         return ((f(NL05Line.DepartureTime),
                  f(NL05Line.ArrivalTime)), )
      elif n == 2:
         return ((f(NL05Line.DepartureTime), f(NL05Line.AR1)),
                 (f(NL05Line.DE1), f(NL05Line.ArrivalTime)))
      elif n == 3:
         return ((f(NL05Line.DepartureTime), f(NL05Line.AR1)),
                 (f(NL05Line.DE1), f(NL05Line.AR2)),
                 (f(NL05Line.DE2), f(NL05Line.ArrivalTime)))
      elif n == 4:
         return ((f(NL05Line.DepartureTime), f(NL05Line.AR1)),
                 (f(NL05Line.DE1), f(NL05Line.AR2)),
                 (f(NL05Line.DE2), f(NL05Line.ArrivalTime)), (0, 0))
      else:
         return (None, None)


   def leg_index(self, number):
      if number < 1 or number > 5:
         raise exceptions.ValueError, "Invalid number given for leg index."
      if number == 1:
         return NL05Line.Leg1
      elif number == 2:
         return NL05Line.Leg2
      elif number == 3:
         return NL05Line.Leg3
      elif number == 4:
         return NL05Line.Leg4
      else:
         return NL05Line.Leg5


   def legTags(self, line, legNb):
      """Format of leg name is 'Leg_AC_8300_YEG_7'."""
      if legNb < 1 or legNb > 4:
         raise exceptions.ValueError, 'Invalid index for flight leg.'
      nbOfLegs = int(line[NL05Line.NbOfLegs])
      dayOfWeek = int(line[NL05Line.DayOfWeek])
      times = self.timeFieldsIndexes(nbOfLegs)
      times_list = []
      for u in times:
         times_list.extend(u)
      # retrieve the leg information
      legsStrings = []
      prev_departure_time = 0
      for i in range(nbOfLegs):
         _leg = line[self.leg_index(i+1)]
         if _leg == NL05Line.VoidLeg:
            raise ValueError, "Invalid leg descriptor found."
         dep_airport = _leg[:3].strip()
         arr_airport =  _leg[3:6].strip()
         airline = _leg[6:8].strip()
         flight_nb = _leg[8:].strip()
         (dep_time, arr_time) = times[i]
         day_offset = self.weekDayOffset(times_list[:(i+1)*2])
         # Use of modulo conversion so we stay in the range of
         # 1 to 7 days (Monday to Sunday, inclusively)
         #
         departure_day = (dayOfWeek + day_offset) % 7
         if departure_day == 0: departure_day = 7
         legsStrings.append( "_".join(["Leg",
                                       airline,
                                       flight_nb,
                                       dep_airport,
                                       str(departure_day)]) )
      return legsStrings


   def weekDayOffset(self, times):
      """Returns the weekday offset (int) due to time runaround the
      clock. Recursive function."""
      if not times or len(times) == 1:
         return 0
      delta = (int(times[-1]) < int(times[-2])) and 1 or 0
      return delta + self.weekDayOffset(times[:-1])


   def listOfLegs(self):
      legs = []
      nbOfLegs = self.line[NL05Line.NbOfLegs]
      if nbOfLegs == '':
         raise exception.ValueError, 'Invalid value for NbOfLegs!!!'
      return self.legTags(self.line, int(nbOfLegs))
      

   def itineraryFromLine(self):
      """Computes the itinerary with leg tags as generated by the
      FlightScheduler program, e.g.

      'Leg_AC_8300_YEG_5', 'Leg_AC_120_YUL_6', ...
      """
      return (self.getKey(), ', '.join(self.listOfLegs()))


   def getItinerary(self):
      """Returns the itinerary computed when the class was
      instantiated."""
      return self.itinerary


   def getKey(self):
      """Returns the key for the itinerary storage purposes."""
      orig = self.airportName(line[NL05Line.Origin])
      dest = self.airportName(line[NL05Line.Destination])
      return "/".join([orig, dest])


   def keyAndItinerary(self):
      """Returns a tuple consisting of the key and the itinerary:
      (key, itinerary)

      The key has the form 'Airport_YUL/Airport_YYC' and describes the
      origin destination airports.
      """
      return (self.key, self.itinerary)


   def visitedAirports(self, legs):
      """Returns the list of airports 3-letter ID (IATA) that were
      visited the flight."""
      n = len(legs)
      iata = []
      # At least, a flight is made of one leg...
      iata.append(legs[0][0:3])
      for i in range(n):      
         leg = legs[i]
         if leg != NL05Line.VoidLeg:
            iata.append(leg[3:6])
         else:
            iata.append(NL05Line.NoAirport)
      return iata


   def getKey(self):
      """
      """
      orig = self.airportName(self.line[NL05Line.Origin])
      dest = self.airportName(self.line[NL05Line.Destination])
      return self.buildKey(orig, dest)


   def buildKey(self, orig, dest):
      """
      Builds the key for retrieval in a database, based on the names
      designated by orig and dest.
      """
      return orig + "/" + dest


   def flightTag(self, line, flightText):
      """
      Returns the flight tag in this format "AC_4320".
      """
      flightInfo = flightText.split(' ')
      flightInfo = [f for f in flightInfo if len(f)>0]
      return "_".join(flightInfo)


   def indexString(self, index):
      """ Returns the string representation of a Line index."""
      if index == NL05Line.Flight1:
         return "Flight1"
      elif index == NL05Line.Flight2:
         return 'Flight2'
      elif index == NL05Line.Flight3:
         return 'Flight3'
      elif index == NL05Line.Origin:
         return 'Origin'
      elif index == NL05Line.Connection1:
         return 'Connection1'
      elif index == NL05Line.Connection2:
         return 'Connection2'
      elif index == NL05Line.Destination:
         return 'Destination'
      elif index == NL05Line.DepartureTime:
         return 'DepartureTime'
      elif index == NL05Line.ArrivalTime:
         return 'ArrivalTime'
      elif index == NL05Line.ElapsedTime:
         return 'ElapsedTime'
      elif index == NL05Line.Plane1:
         return 'Plane1'
      elif index == NL05Line.Plane2:
         return 'Plane2'
      elif index == NL05Line.Plane3:
         return 'Plane3'
      elif index == NL05Line.DayOfWeek:
         return 'DayOfWeek'
      elif index == NL05Line.Base:
         return 'Base'
      elif index == NL05Line.Dem:
         return 'Dem'
      elif index == NL05Line.Rev:
         return 'Rev'
      elif index == NL05Line.NbOfLegs:
         return 'NbOfLegs'
      elif index == NL05Line.Leg1:
         return 'Leg1'
      elif index == NL05Line.Leg2:
         return 'Leg2'
      elif index == NL05Line.Leg3:
         return 'Leg3'
      elif index == NL05Line.Leg4:
         return 'Leg4'
      elif index == NL05Line.Leg5:
         return 'Leg5'
      elif index == NL05Line.Spill:
         return 'Spill'
      elif index == NL05Line.Rec:
         return 'Rec'
      elif index == NL05Line.Mark:
         return 'Mark'
      elif index == NL05Line.AR1:
         return 'AR1'
      elif index == NL05Line.DE1:
         return 'DE1'
      elif index == NL05Line.AR2:
         return 'AR2'
      elif index == NL05Line.DE2:
         return 'DE2'
      elif index == NL05Line.AIRPORTS:
         return 'AIRPORTS'
      elif index == NL05Line.LineLength:
         return 'LineLength'
      elif index == NL05Line.VoidLeg:
         return 'VoidLeg'
      elif index == NL05Line.VoidTime:
         return 'VoidTime'
      else:
         return 'BAD VALUE!!!'

#
#-- End of "class NL05Line"


#-----------------------------------------------------
#
#  Class NL05Data
#
#-----------------------------------------------------

class NL05Data():
   """
   This class implements the parsing of a NetLine2005
   itineraries file.
   """

   def __init__(self, file=None):
      """
      Constructor.

      Optionally accepts a file to be parsed. This file is assumed to
      be an itineraries file in the NetLine (2005) format.
      """
      self.file = file
      self.lines = []
      self.content = {}


   def matchingLines(self, criterion):
      """
      Returns a list of lines that are matching a specific criterion.
      """
      lines = []
      if not utils.isValidFile(self.file):
         raise exceptions.IOError, \
             'Invalid file. Please provide with a valid file.'
      m = len(criterion)
      # Scan the entife file, one line at the time
      for line in fileinput.input(self.file):
         # Parsing of the line by a NL05Line object
         nl = NL05Line(line)
         V = True
         for i in range(m):
            V = V and (nl[criterion[i][0]] == criterion[i][1]) or False
         if V:
            lines.append(line)

      return sorted(lines)


   def processFile(self, file=None):
      """
      Processes the specified file in argument.

      If the file is not valid, throws an IOError.
      """
      print '-> Reading the NetLine itineraries file:'
      print os.path.abspath(file)
      count = 0
      if not file or file is None:
         file = self.file
      else:
         self.file = file
      if not utils.isValidFile(file):
         raise exceptions.IOError, \
             'Invalid file. Please provide with a valid file.'
      # Process the entire file, one line at the time
      self.demandDict = {}
      for line in fileinput.input(file):
         if line:
            # Parsing of the line by a NL05Line object
            nl = NL05Line(line)
            self.lines.append(nl)
            count = count + 1

            # Retrieving the key and itinerary from the parser object
            key, itin = nl.keyAndItinerary()
            self.demandDict[itin] = nl.getDemand()
            if itin is not None and key is not None:
               # Updating the self.content dictionnary
               if key not in self.content:
                  self.content[key] = [itin]
               else:
                  self.content[key].append(itin)
                  x = self.content[key]
                  self.content[key] = list(set(x))
      print 'Read %d lines in the input file.' % count
      print


   def demandDictionnary(self):
      return self.demandDict.copy()


   def displayContent(self):
      """
      Displays the content of the NL05Data class (after an itinerary
      file was parsed).
      """
      print '--------------------------------'
      print 'Printout of the NL05Data object.'
      print
      for key in self.content.keys():
         print str(key)
         print
         for itin in self.content[key]:
            print str(itin)
         print
         print
      print '--------------------------------'
      print
      print 'Content is clustered into ' + str(len(self.content)),
      print ' categories (or keys).'
      count = self.__countItineraries(self.content)
      print 'A total of ' + str(count) + ' itineraries are listed.'


   def connectingTimes(self):
      """
      Returns a dictionnary with the minimum and maximum connection
      time for any given airport.

      example:
      > nlData = NL05Data()
      > nlData.processFile(fileName)
      > d = nlData.connectingTimes()
      > print d["YUL"]
      (45, 122)

      NOTE: The values showned are arbitrary and do not reflect
      reality.

      """
      connTimes = {}
      MIN_POS = 0
      MAX_POS = 1
      for nl_line in self.lines:
         for (station, connTime) in nl_line.connectionTimes():
            if not connTimes.has_key(station):
               connTimes[station] = (connTime, connTime,)
            min_time = min(connTimes[station][MIN_POS],connTime)
            max_time = max(connTimes[station][MAX_POS],connTime)
            connTimes[station] = (min_time, max_time,)
      return connTimes


   def hasItinerary(self, airports, itin):
      """
      Tells if the itinerary, for the given airports pair, is present
      in the data.
      Returns True if data is found, False if not.
      """
      if airports not in self.content:
         return False
      if itin not in self.content[airports]:
         return False
      return True


   def itinerariesDictionnary(self):
      """Returns the itineraries dictionnary.

      The key is the Origin/Destination description string and the
      itinerary is a list fo strings, each string in the list being an
      itinerary.

      For example:

      >> d = NL05Data()
      >> d.processFile('itineraries.in')
      >> itins = d.itinerariesDictionnary()
      >> for key in itins:
      ...    for itinerary in itins[key]:
      ...        print str(itinerary)

      (snippet)

      >> p = itins['Airport_YVR/Airport_YUL']
      >> len(p)
      ...

      """
      return self.content.copy()


   def printItineraries(self, itineraries):
      """
      Print the specified itineraries (dictionnary format) to the
      standard output.

      Input: itineraries, a dictionnary containing the itineraries
      (list of strings) grouped by origin/destination (key).

      """
      for k in itineraries.keys():
         print k
         for i in range(len(itineraries[k])):
            print itineraries[k][i]
         print


   def __countItineraries(self, data):
      """
      Returns the number of all itineraries in the dictionnary.
      """
      count = 0
      for key in data.keys():
         count = count + len(data[key])
      return count
#
#-- End of "class NL05Data"


#-----------------------------------------------------
#
#  Script's specific functions and methods
#
#-----------------------------------------------------

def argumentFile(file=None):
   """
   Gets the itinerary file (*.in) given in argument, makes
   some routine checks about this file, and returns it.

   Throws an Exception
   """
   if file is None:
      file = sys.argv[1]
   if not os.path.exists(file):
      raise exceptions.IOError, 'File to be parsed does not exists. '\
      'Please give a valide file to be parsed.'
   if not os.path.isfile(file):
      raise exceptions.IOError, 'File given is not a file. '\
          'Please indicate a file to be parsed.'
   return file


def findMatchingLines():
   try:
      file = argumentFile()
      if not utils.isValidFile(file):
         raise exceptions.IOError, 'Invalid file.'
   except exceptions.IOError:
      print 'Please provide with a valid file.'
      return -1
   print 'File being treated: ' + str(file)

   # Instantiate a NL05Data object to manipulate
   # the NL05 data.
   #
   nlData = NL05Data(file)
   criterion = [(NL05Line.Origin, 'YYZ'), (NL05Line.Destination, 'YVR')]
   for item in nlData.matchingLines(criterion):
      print str(item)
   return 0
   

def inputFile(file=None):
   try:
      file = argumentFile(file)
      if not utils.isValidFile(file):
         raise exceptions.IOError, 'Invalid file.'
   except exceptions.IOError:
      print 'Please provide with a valid file.'
      return -1
   print 'File being treated: ' + str(file)
   return file


def main(arguments):
   """
   Proceed with the extraction of information in an itinerary file
   (*.in) from NetLine (2005).
   """
   itinFile = arguments['itinFile']
   # Instantiate a NL05Data object to manipulate
   # the NL05 data.
   #
   nlData = NL05Data(itinFile)
   nlData.processFile(itinFile)
   if arguments['connections']:
      print "Proceeding with compilation of connection times..."
      conn_times = nlData.connectingTimes()
      utils.saveDict("connectionTimes.txt", conn_times)
   else:
      # Ask the NL05Data object to load the itineraries from
      # the designated file.
      #
      print "Retrieving the itineraries dictionnary."
      itins = nlData.itinerariesDictionnary()
   return 0


def testNL05Data(args_dict):
   import os.path
   netlineFile = args_dict["itinFile"]
   if not os.path.isfile(netlineFile):
      raise exceptions.ValueError, "Please provide with a valide file."
   netLineData = NL05Data()
   netLineData.processFile(netlineFile)
   netline_itins = netLineData.itinerariesDictionnary()
   print "-- Testing NL05Data object --"
   #netLineData.displayContent()
   #itins = netline_itins.values()
   #for itin in itins:
   #   print itin
   #print "-----------------------------"   
   #print "Number of itineraries read = %d" % len(itins)


def testNL05Line(args_dict):
   import os.path
   nl_file = args_dict["itinFile"]
   if not os.path.isfile(nl_file):
      raise exceptions.ValueError, "Please provide with a valide file."
      return -1
   count = 0
   for line in fileinput.input(nl_file):
      nl = NL05Line(line)
      print nl.itinerary
      count += 1
   print "Itineraries read: " + str(count)


def usage():
    """
    Describes the usage of this script, with arguments descriptions.
    """
    print 'Please use one of the following arguments in this manner:'
    print
    print ' --itin-file'
    print '     File containing the itineraries to parse.'
    print
    print ' --demand, -d'
    print '     Saves the computed total demand information into'
    print '     the file specified by this option.'
    print
    print ' --verbose, -v'
    print '     Display all the total demands information'
    print '     to the standard output.'
    print
    print ' --connections, -c'
    print '     Compiles the connection times for each airport (max and min)'
    print '     and saves that info into a text file.'
    print
    print ' --test, -t'
    print '     Tests the NL05Line class on a specified itineraries file.'
    print
    print ' --help, -h'
    print '     Displays this message.'
    print
    return 0


def parseArguments(inputArgs):
   """
   Parses the given list of arguments (string) given by inputArgs and
   returns a dictionnary with  corresponding key/values.
   """
   options = 'itin-file= help demand= verbose test connections'
   optionsList = options.split()
   shortOptions = 'hdtvc'   
   try:
      optlist, args = getopt.getopt(inputArgs, shortOptions, optionsList)
   except getopt.GetoptError, err:
      print str(err)
      usage()
      sys.exit(2)
   if not optlist:
      usage()
      sys.exit(2)
   arguments = {}
   arguments['itinFile'] = None
   arguments['verbose'] = False
   arguments['connections'] = False
   arguments['testing'] = False
   for option, answer in optlist:
      if option in ("-h", "--help"):
         sys.exit(usage())
      elif option in ("-v", "--verbose"):
         arguments['verbose'] = True
      elif option in ("-c", "--connections"):
         arguments['connections'] = True
      elif option == '--itin-file':
         arguments['itinFile'] = answer
      elif option in ('--test', '-t'):
         arguments['testing'] = True
      else:
         print "Option %s not recognized..." % option
         assert False, "unhandled option"
   return arguments


#------------------------------------------------------------#
if __name__ == "__main__" : 
   args = parseArguments(sys.argv[1:])
   if args["testing"]:
      #testNL05Line(args)
      testNL05Data(args)
   else:
      sys.exit(main(args))

#------------------------------------------------------------#
