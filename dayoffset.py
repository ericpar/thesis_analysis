#! /usr/bin/python
# -*- coding: utf-8 -*-
#
"""Author: Eric Parent <eric.parent@gerad.ca>

License: GPL v3.0 or later

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""
import sys

def weekDayOffset(times):
    """
    Returns the weekday offset (int) due to time runaround the clock.
    Recursive function.
    """
    if not times or len(times) == 1:
        return 0
    delta = (int(times[-1]) < int(times[-2])) and 1 or 0
    
    return delta + weekDayOffset(times[:-1])


def UpdateMyDict(d):
    b = {}
    b['orange'] = -1
    b['bleu'] = -2
    b['rouge'] = -3
    return d.update(b)


def OperateMyDict(d):
    d['orange'] = 3
    d['bleu'] = 2
    d['rouge'] = 1
    #return d


#------------------------------#
if __name__ == "__main__" : 
    d = {}
    d['patate'] = 5
    d['foret'] = 6
    print 'Updated dict:'
    print str(UpdateMyDict(d))
    print

    d = {}
    d['patate'] = 5
    d['foret'] = 6
    print 'Modified dict:'
    OperateMyDict(d)
    print str(d)
    print

    #times = [2300, 2350, 0110, 0650]
    #print 'Offset computed = ' + str(weekDayOffset(times))
    #times.reverse()
    #print 'Offset computed = ' + str(weekDayOffset(times))
    sys.exit(0)
   #sys.exit( main() )
#------------------------------#
