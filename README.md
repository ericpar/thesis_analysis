# Description #

These files are Python scripts that I've been putting up together to manipulate and analyse the numerical results I've gathered while working on my master's thesis.

I've been using these scripts for partial analysis, information gathering and also generating parts of the LaTeX tables I was using in my thesis document.

I'm lazy and I tend to get a computer do the work for me whenever I can.

# Purpose #

These files hare not meant to be a framework or toolbelt of any kind go anybody else but me. Sorry.

But if you find some inspiration while reading these files, that's awesome and please feel free to ping me.

Regards,

Eric