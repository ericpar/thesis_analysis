#! /usr/bin/python
# -*- coding: utf-8 -*-
"""
Author: Eric Parent <eric.parent@gerad.ca>

License: GPL v3.0 or later

Copyright (C) 2008 Eric Parent

This file is part of 'thesis_analysis'.

Foobar is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or (at your option) any later version.

Foobar is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see http://www.gnu.org/licenses/.

You can also see the details in the file 'gpl-3.0.txt' that is bundled
with the current file.

"""

import os
import sys
import getopt
import fileinput
import consts
from PIG_AnalysisTools import *



def usage():
    """
    Describes the usage of this script, with arguments descriptions.
    """
    print
    print 'Please use one of the following arguments in this manner:'
    print
    print ' --pig-file='
    print '       Designated thePIG file with generated itineraries.'
    print
    print ' --netline='
    print '       Describes the NetLine itinerary file to be used.'
    print
    print ' --help, -h'
    print '       Displays this message.'
    print
    return consts.SUCCES



def parseOptions(inArgs):
    """
    @brief Parses the options and return the values in a dictionnary.
    The fields are the following:

    @return a dictionnary with the appropriate values for options
    (keys of the dictionnary).

    """
    options = 'help pig-file= netline='
    optionsList = options.split()
    shortOptions = 'h'
    try:
        optlist, args = getopt.getopt(inArgs, shortOptions, optionsList)
    except getopt.GetoptError, err:
        print str(err)
        usage()
        sys.exit(2)
    if not optlist:
        usage()
        sys.exit(2)
    arguments = {}
    arguments['netlineFile'] = None
    arguments['pigFile'] = None
    for option, answer in optlist:
        # help
        if option in ("-h", "--help"):
            sys.exit(usage())
        # demand-shelve
        elif option == '--netline':
            arguments['netlineFile'] = answer
        # pig-file
        elif option == '--pig-file':
            arguments['pigFile'] = answer
        else:
            usage()
            assert False, "unhandled option"
    return arguments


def main(argsDict):
    """

    INPUT:

    - argsDict, a dict containing all the options (keys) and values
    (values) for this application.

    """
    demandCompiler = DemandCompiler(argsDict)
    demandCompiler.displayServicesRatio()
    demandCompiler.displayUnaddressedServices()
    return consts.SUCCES
    


###################################################################

if __name__ == "__main__" : 
    """
    Performs the comparison of the generated itineraries from PIG
    module where they have been processed and saved in a "shelve".
    The reference itineraries are found in the NetLine filel

    """
    sys.exit( main( parseOptions(sys.argv[1:]) ) )

###################################################################
